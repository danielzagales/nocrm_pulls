import requests
from datetime import datetime
import os
import sys
from google.cloud import bigquery

# noCRM vars
base_url = 'https://pandera-systems.nocrm.io/api/v2/'
headers = {'X-API-KEY': os.environ["nocrm_key"]}
params = {'limit': '3000'}

# gcp vars
dataset_id = 'nocrm_raw'
record_time = datetime.now().strftime("%Y-%m-%d")
endpoints = ['leads','users']

client = bigquery.Client()

def pull_nocrm(endpoint):
    o_response = requests.get(base_url+endpoint, headers=headers, params=params)

    try:
        o_response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        return("Error: " + str(e))

    j_response = o_response.json()
    print("Success: " + endpoint + ", " + len(j_response) "records extracted")
    return(j_response)

def json_parse(jsonData):
    rows = []

    for row  in jsonData:
        if 'remind_time' in row.keys() and row['remind_time'] is not None:
            row['remind_time'] = row['remind_time'] + ":00"
        row.update({"record_time": record_time})
        # still need to figure out how to address issue with team field in users pull
        if 'teams' in row.keys():
            row['teams'] = []
        rows.append(row)

    print("Success: JSON parsed " + len(rows) + " records in parsed data")
    return(rows)

def insert_bq(jsonData,endpoint):
    table_ref = client.dataset(dataset_id).table('nocrm_' + endpoint)
    table = client.get_table(table_ref)

    errors = client.insert_rows_json(table,jsonData)

    print(errors)

def update_rt():
    update_query = (
        "UPDATE `folkloric-clock-161420.nocrm_raw.nocrm_latest_rt` "
        "SET record_time = '" + record_time +
        "' WHERE 1=1"
    )
    query_job = client.query(update_query,location="US")
    print("Success: Record Time set to: " + record_time)

def main(param):
    for endpoint in  endpoints:
        jsonData = pull_nocrm(endpoint)
        cleanData = json_parse(jsonData)
        insert_bq(cleanData, endpoint)
        print("Success: " + endpoint " complete")
    update_rt()


# main()
